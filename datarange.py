from datetime import date, timedelta


def daterange(
        start_date: date,
        stop_date: date,
        step: int = 1,
):
    current_date = start_date
    while current_date < stop_date:
        yield current_date
        current_date += timedelta(days=step)


start = date.today()
stop = date(2023, 1, 1)

# for i in daterange(start, stop):
#    print(i)

def multi_yield():
    yield_str = "first yield"
    yield yield_str
    yield_str = "second yield"
    yield  yield_str



