# дивіться за посиланням у readme.md - блокноти в colab

import sys

nums_squared_lc = [i ** 3 for i in range(100000)]

nums_squared_gc = (i ** 3 for i in range(100000))

print(f"size of list: {sys.getsizeof(nums_squared_lc)}")
print(f"size of  gen: {sys.getsizeof(nums_squared_gc)}")